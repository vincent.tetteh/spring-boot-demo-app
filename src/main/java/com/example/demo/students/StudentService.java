package com.example.demo.students;

import org.apache.catalina.LifecycleState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentService {

    List<Students> getStudents(){
        return List.of(
                new Students("Vincent",90,Gender.MALE),
                new Students("Kwame",12,Gender.MALE),
                new Students("Yaw", 36, Gender.MALE),
                new Students("Yaa",18,Gender.FEMALE),
                new Students("Joyce",25,Gender.FEMALE)
        );
    }



}
