package com.example.demo.students;

public class Students implements StudentInterface{
    private String name;
    private int age;
    private Gender gender;

    public Students(String name, int age, Gender gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Students{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }

    @Override
    public String studentsWelcomeMessage(String name) {
        return "You are welcome " + name;
    }

    @Override
    public String studentExitMessage(String name) {
        return "Goodbye " + name;
    }
}
