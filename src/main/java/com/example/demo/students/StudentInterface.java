package com.example.demo.students;

public interface StudentInterface {
    String studentsWelcomeMessage(String name);
    String studentExitMessage(String name);

}
